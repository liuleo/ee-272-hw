# This script performs a design-space exploration for ResNet-18
# Please see the end of the file to look at the steps in the script
# 1. Iterate through PE counts of [16, 64, 256]
# 2. Iterate through combinations of the following 3-level memory capacities:
#     - RF size = [16, 32, 64, 128, 256, 512]
#     - Buffer size = [8192, 16384, 32768, 65536, 131072, 262144, 524288]
#     - DRAM size = 1073741824
#		 Architecrures with area less than 2mm^2 are ignored.
# 3. Compute best figure-of-merit for ResNet-18 layer conv1 with run_optimizer.py
#       FoM = runtime * energy
# 6. Run tiling optimization for all layers.

import json
import csv
import os
import subprocess
import re
import sys


ROOT_DIR = "{}/..".format(os.path.dirname(os.path.realpath(__file__)))
ARCH_DIR = "{}/arch".format(ROOT_DIR)
LAYER_DIR = "{}/layer".format(ROOT_DIR)
SCHEDULE_DIR = "{}/schedule".format(ROOT_DIR)

OPT_OUTPUT_DIR = "{}/output".format(ROOT_DIR)
RES_DIR = "{}/results".format(ROOT_DIR)

AREA_SRAM_BIT_CELL  = 0.0000002                     # for calculating buffer size
AREA_SINGLE_BIT_REG = 0.000002                      # for calculating PE register file size
AREA_16b_MAC_UNIT = 1000 * AREA_SINGLE_BIT_REG      # for calculating PE array size

REG_ACCESS_COSTS = {
    16: 0.03,
    32: 0.06,
    64: 0.12,
    128: 0.24,
    256: 0.48,
    512: 0.96
}

BUFFER_ACCESS_COSTS = {
    8192: 4,
    16384: 6,
    32768: 9,
    65536: 13.5,
    131072: 20.2,
    262144: 30.4,
    524288: 45.6
}

# Given a 3-level architecture (as dict), compute the area. Assumes architecture
# is of the form [x, 1, 1]
def compute_area(arch):
    mem_levels = arch["mem_levels"]
    capacity = arch["capacity"]
    parallel_count = arch["parallel_count"]

    assert(len(capacity) == len(parallel_count) == mem_levels == 3)

    num_PEs = parallel_count[0]
    total_MAC_area = sum([AREA_16b_MAC_UNIT for _ in range(num_PEs)])
    total_RF_area = sum([2 * capacity[0] * 8 * AREA_SINGLE_BIT_REG for _ in range(num_PEs)])
    total_buffer_area = 2 * capacity[1] * 8 * AREA_SRAM_BIT_CELL

    total_area = total_MAC_area + total_RF_area + total_buffer_area
    return total_area, total_MAC_area, total_RF_area, total_buffer_area


# Produce a list of architectures that span the design space (writes out JSON to
# arch/)
def gen_3level_design_space():
    print("Generating 3-level design space...")

    archs = list()
    file_suffix = 0
    for num_PEs in [16, 64, 256]:
        for rf_cap in [16, 32, 64, 128, 256, 512]:
            for buffer_cap in [8192, 16384, 32768, 65536, 131072, 262144, 524288]:
                dram_cap = 1073741824

                arch = {
                    "mem_levels": 3,
                    "capacity": [rf_cap, buffer_cap, dram_cap],
                    "access_cost": [REG_ACCESS_COSTS[rf_cap], BUFFER_ACCESS_COSTS[buffer_cap], 200],
                    "parallel_count": [num_PEs, 1, 1],
                    "parallel_cost": [0.035],
                    "precision": 16
                }

                area, area_MAC, area_RF, area_buffer = compute_area(arch)
                if area > 2:
                    continue

                archs.append(arch)

                with open("{}/arch_3level_{}.json".format(ARCH_DIR, file_suffix), "w") as f:
                    f.write(json.dumps(arch, indent=4))
                    file_suffix += 1

    print("{} arch design files generated.".format(file_suffix))
    return archs


# Iteratively execute optimizer for conv1 on list of 3-level architectures 
# (writes out to output/)
def iterate_3level_design_space(archs):
    print("Iterating through 3-level design space...")

    file_suffix = 0
    for arch in archs:
        if arch["parallel_count"][0] >= 256:
            subprocess.run([
                "python",
                "{}/../ee272-interstellar/tools/run_optimizer.py".format(ROOT_DIR),
                "basic",
                "{}/arch_3level_{}.json".format(ARCH_DIR, file_suffix),
                "{}/resnet18-conv1.json".format(LAYER_DIR),
                "-s",
                "{}/schedule_C_K_replication_3x16_level0.json".format(SCHEDULE_DIR),
                "-v"
            ])
        elif arch["parallel_count"][0] >= 64:
            subprocess.run([
                "python",
                "{}/../ee272-interstellar/tools/run_optimizer.py".format(ROOT_DIR),
                "basic",
                "{}/arch_3level_{}.json".format(ARCH_DIR, file_suffix),
                "{}/resnet18-conv1.json".format(LAYER_DIR),
                "-s",
                "{}/schedule_C_K_replication_3x8_level0.json".format(SCHEDULE_DIR),
                "-v"
            ])
        else:
            subprocess.run([
                "python",
                "{}/../ee272-interstellar/tools/run_optimizer.py".format(ROOT_DIR),
                "basic",
                "{}/arch_3level_{}.json".format(ARCH_DIR, file_suffix),
                "{}/resnet18-conv1.json".format(LAYER_DIR),
                "-s",
                "{}/schedule_C_K_replication_3x4_level0.json".format(SCHEDULE_DIR),
                "-v"
            ])
        file_suffix += 1

    subprocess.run("mv result_*.json {}".format(OPT_OUTPUT_DIR), shell=True)

    print("Optimizer iteration completed (results written to {}".format(OPT_OUTPUT_DIR))


# Find optimal 3-level architecture from given results from iteration (writes
# out CSV to output/)
def find_best_design():
    print("Finding best design...")

    runtimes = list(); energies = list()
    best_fom = None; best_arch = None; best_result = None; best_areas = None; best_result_file = None
    regex = re.compile("result_.*")
    with open("{}/results.csv".format(OPT_OUTPUT_DIR), "w", newline="") as csvfile:
        writer = csv.writer(csvfile, delimiter=",")
        results = list()
        for root, dirs, files in os.walk(OPT_OUTPUT_DIR):
            for file in files:
                if regex.match(file):
                    with open("{}/{}".format(OPT_OUTPUT_DIR, file), "r") as f:
                        result = json.load(f)
                        results.append(result)
                    with open("{}".format(result["file_arch"]), "r") as f:
                        arch = json.load(f)
                        area, area_MAC, area_RF, area_buffer  = compute_area(arch)

                    fom = float(result["energy"]) * float(result["runtime"])
                    energies.append(result["energy"])
                    runtimes.append(result["runtime"])

                    if best_fom is None or fom < best_fom:
                        best_fom = fom
                        best_arch = arch
                        best_result = result
                        best_areas = (area, area_MAC, area_RF, area_buffer)
                        best_result_file = "{}/{}".format(OPT_OUTPUT_DIR, file)

                    writer.writerow([result["file_arch"], result["file_layer"], result["energy"], result["runtime"], area, area_MAC, area_RF, area_buffer, fom])

    print("Best design: {}".format(best_result["file_arch"]))
    print("\tRuntime: {}".format(best_result["runtime"]))
    print("\tEnergy: {}".format(best_result["energy"]))
    print("\tFoM (runtime * energy): {}".format(best_fom))
    print("\tArea: {}".format(best_areas[0]))
    print("All results written as CSV to {}/results.csv".format(OPT_OUTPUT_DIR))

    return best_arch, best_result, best_areas, best_result_file

# From previous optimization, we know its 3-level with 256 PEs, so this is a
# little hardcoded, oops.
def all_conv_layers(arch):
    print("Running script for all layers:")

    layerfiles = [
				"resnet18-conv1.json",
        "resnet18-conv2.json",
        "resnet18-conv3_stride2.json",
        "resnet18-conv3.json",
        "resnet18-conv4_stride2.json",
        "resnet18-conv4.json",
        "resnet18-conv5_stride2.json",
        "resnet18-conv5.json"
    ]

    for layerfile in layerfiles:
        print(layerfile)

        schedule = "{}/schedule_C_K_replication_3x16_level0.json".format(SCHEDULE_DIR)
        if layerfile != "resnet18-conv1.json":
            schedule = "{}/schedule_C_K_replication_16x16_level0.json".format(SCHEDULE_DIR)

        subprocess.run([
            "python",
            "{}/../ee272-interstellar/tools/run_optimizer.py".format(ROOT_DIR),
            "basic",
            "{}/best_arch.json".format(RES_DIR),
            "{}/{}".format(LAYER_DIR, layerfile),
            "-s",
            schedule,
            "-v" 
        ])

        subprocess.run("mv result_*.json {}".format(RES_DIR), shell=True)

    print("All other conv layers completed (results written to {}".format(RES_DIR))


if __name__ == "__main__":
    # If another argument is provided, skip architecture optimization.
    if len(sys.argv) < 2:
        subprocess.run("rm {}/*.json".format(ARCH_DIR), shell=True)
        subprocess.run("rm {}/*.json".format(OPT_OUTPUT_DIR), shell=True)
        subprocess.run("rm {}/*.csv".format(OPT_OUTPUT_DIR), shell=True)
        subprocess.run("rm {}/*.json".format(RES_DIR), shell=True)
        subprocess.run("rm {}/*.png".format(RES_DIR), shell=True)

        archs_3 = gen_3level_design_space()
        iterate_3level_design_space(archs_3)

    best_arch_3level, best_res_3level, best_area_3level, best_file_3level = find_best_design()

    print("\nJSON of best architecture:")
    print(json.dumps(best_arch_3level, indent=4))
    print("\nOptimizer result:")
    print(json.dumps(best_res_3level, indent=4))
    print("\nArea (total, MAC area, RF area, buffer area): {}".format(best_area_3level))

    with open("{}/best_arch.json".format(RES_DIR), "w") as f:
        f.write(json.dumps(best_arch_3level, indent=4))

    all_conv_layers(best_arch_3level)