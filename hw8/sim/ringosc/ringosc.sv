`timescale 1ns/1fs

module ringosc (
    input real vdd,
    output var logic out
);
    // YOUR IMPLEMENTATION HERE
    // f = A * vdd + b
    //  A = 3.4GHz/V
    //  b = -3.9
    //  0.5GHz <= f <= 2.0GHz
    //  0.5ns <= T <= 2ns
    //  0.25ns <= half_T <= 1ns

    real A = 3.4;
    real B = -3.9;
    real f, half_T;
    
    var logic out_next;

    always @(vdd) begin
        f = A * vdd + B;
        if (f < 0.5) begin
            half_T = 1;
        end else if (f > 2.0) begin
            half_T = 0.25;
        end else begin
            half_T = 1/(2*f);
        end
    end

    always @(out_next) begin
        out = #half_T out_next;
    end

    always @(out) begin
        out_next = ~out;
    end


    initial begin
        half_T = 0.25;
        out = 0;
        out_next = 1;
    end
    
endmodule

