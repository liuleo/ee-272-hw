`timescale 1s/1fs

module filter #(
    parameter real tau=50e-9
) (
    input real in
);
    // ADD VARIABLE DECLARATIONS HERE AS NECESSARY
    real start_t;
    real start_y;
    real now, dt;
    real last_in;

    function real out();
        // FILL IN FUNCTION IMPLEMENTATION
        now = $realtime;
        dt = now - start_t;
        return $exp(-1*dt/tau)*start_y + (1-$exp(-1*dt/tau))*in;
    endfunction

    always @(in) begin
        // UPDATE INTERNAL STATE VARIABLES AS NECESSARY
        now = $realtime;
        dt = now - start_t;

        start_y = $exp(-1*dt/tau)*start_y + (1-$exp(-1*dt/tau))*last_in;

        start_t = now;
        last_in = in;
    end

    initial begin
        start_t = 0;
        start_y = 0;
        last_in = 0;
    end
endmodule
