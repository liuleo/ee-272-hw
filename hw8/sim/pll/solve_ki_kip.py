from math import pi, atan, tan
from sympy import Symbol, solve

# params from pll.m
Kvco = 2*pi*3.4e9   # in rad/s/V, not Hz/V!
Vdd = 1.8
Ndiv = 10
tau_f = 15.9e-9

# 1. s = jw; f = 1MHz @ unity
f = 1E6
w = 2 * pi * f
s = 1j * w

# 2. L = (K0 * Kp) + (K1 * Ki)
K0 = (Kvco/Ndiv) * (1/s) * (Vdd/pi) * 1/(1+s*tau_f)
K1 = (Kvco/Ndiv) * (1/s) * (Vdd/pi) * 1/(1+s*tau_f) * (1/s)
print({'K0': K0, 'K1': K1})

# 3. As well: L = jx + y => |L| = 1, phase(L) = -120deg = -2pi/3
x = Symbol('x'); y = Symbol('y')
results = solve([
     x**2 + y**2 - 1,    # = 0
     x/y - tan(-2*pi/3)  # = 0
])
for res in results:
     if res[x] < 0 and res[y] < 0:
          x = float(res[x]); y = float(res[y])
          break
if type(x) is not float or type(y) is not float:
     print("Could not find solution?")
     exit()
print({'x': x, 'y': y})

# 4. Solve (K0 * Kp) + (K1 * Ki) = jx + y
Kp = Symbol('Kp'); Ki = Symbol('Ki');
results = solve([
     K0.imag*Kp + K1.imag*Ki - x,  # = 0
     K0.real*Kp + K1.real*Ki - y,  # = 0
])
Kp = float(results[Kp])
Ki = float(results[Ki])
print({'Kp': Kp, 'Ki': Ki})

# 5. Check answers
L = Kvco * (1/Ndiv) * (1/s) * (Vdd/pi) * (1/(1+tau_f*s)) * (Kp + (Ki/s))
print(L, abs(L))
