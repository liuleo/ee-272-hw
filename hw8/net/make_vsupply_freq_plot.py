import matplotlib.pyplot as plt
import numpy as np


vsups     = [1.0,   1.1,    1.2,    1.3,    1.4,    1.5,    1.6,    1.7,    1.8]
Ts_pre    = [3779,  1992,   1264,   912.9,  708.6,  574.6,  490.0,  427.7,  379.9]  # in picosecond
freqs_pre = [(1/(T*1E-12)) * 1E-9 for T in Ts_pre]                                  # in GHz
Ts_post   = [1E100, 3805,   2381,   1695,   1299,   1057,   887.4,  766.1,  679.9]  # in picosecond
freqs_post= [(1/(T*1E-12)) * 1E-9 for T in Ts_post]                                 # in GHz

vsups = np.array(vsups)

plt.plot(vsups, freqs_pre)
plt.xlabel("Supply voltage (V)")
plt.ylabel("Output frequency (GHz)")
plt.show()


a, b, c = np.polyfit(vsups, freqs_post, 2)
fit = a * (np.array(vsups)*vsups) + b * vsups + c

plt.plot(vsups, freqs_post)
plt.plot(vsups, fit, linestyle="--")

plt.xlabel("Supply voltage (V)")
plt.ylabel("Output frequency (GHz)")
plt.show()

