#ifndef INPUT_DOUBLE_BUFFER_H
#define INPUT_DOUBLE_BUFFER_H

template <int size, int IC0, int OC0>
class InputDoubleBufferWriter{
public:
    InputDoubleBufferWriter(){}

    #pragma hls_design interface
    void CCS_BLOCK(run)(ac_channel<Params> &paramsIn,
                        ac_channel<PackedInt<INPUT_PRECISION, 4> > &din,
                        ac_channel<chanStruct<PackedInt<INPUT_PRECISION,IC0>, size> > &dout)
    {
        // -------------------------------
        // Your code starts here

        Params params = paramsIn.read(); // read params

        for (int oy1 = 0; oy1 < params.OY1; oy1++) {
            for (int ox1 = 0; ox1 < params.OX1; ox1++) {
                chanStruct<PackedInt<INPUT_PRECISION, IC0>, size> tile;
                PackedInt<INPUT_PRECISION, 4> inputItem;

                uint_16 IX0 = params.STRIDE * (params.OX0 - 1) + params.FX;
                uint_16 IY0 = params.STRIDE * (params.OY0 - 1) + params.FY;
                uint_16 numTileItems = params.IC1 * IX0 * IY0;
                for (int tileItemIdx = 0; tileItemIdx < numTileItems; tileItemIdx++) {
                    for (int inputItemIdx = 0; inputItemIdx < IC0/4; inputItemIdx++) { // assume IC0 is a power of two >= 4
                        inputItem = din.read(); // read next 4-packed input item
                        for (int i = 0; i < 4; i++) {
                            tile.data[tileItemIdx].value[4*inputItemIdx + i] = inputItem.value[i];
                        }
                    }
                }
                dout.write(tile); // write tile (infers double buffer)
            }
        }

        // Your code ends here
        // -------------------------------
    }
};

template <int size, int IC0, int OC0>
class InputDoubleBufferReader{
public:
    InputDoubleBufferReader(){}

    #pragma hls_design interface
    void CCS_BLOCK(run)(ac_channel<Params> &paramsIn,
                        ac_channel<chanStruct<PackedInt<INPUT_PRECISION, IC0>, size> > &din, 
                        ac_channel<PackedInt<INPUT_PRECISION, IC0> > &dout)
    {
        // -------------------------------
        // Your code starts here

        Params params = paramsIn.read(); // read params
        uint_16 IX0 = params.STRIDE * (params.OX0 - 1) + params.FX;
        uint_16 IY0 = params.STRIDE * (params.OY0 - 1) + params.FY;

        for (int oy1 = 0; oy1 < params.OY1; oy1++) {
            for (int ox1 = 0; ox1 < params.OX1; ox1++) {

                chanStruct<PackedInt<INPUT_PRECISION, IC0>, size> tile = din.read(); // read tile (infers double buffer)

                for (int oc1 = 0; oc1 < params.OC1; oc1++) {
                    for (int ic1 = 0; ic1 < params.IC1; ic1++) {
                        for (int fy = 0; fy < params.FY; fy++) {
                            for (int fx = 0; fx < params.FX; fx++) {
                                for (int oy0 = 0; oy0 < params.OY0; oy0++) {
                                    for (int ox0 = 0; ox0 < params.OX0; ox0++) {
                                        uint_16 ix0 = params.STRIDE * ox0 + fx;
                                        uint_16 iy0 = params.STRIDE * oy0 + fy;
                                        uint_16 adr = ic1 * IX0 * IY0 + iy0 * IX0 + ix0;
                                        dout.write(tile.data[adr]); // write ifmap pixels in correct order for the systolic array to digest
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
    
        // Your code ends here
        // -------------------------------
    }
};

template <int size, int IC0, int OC0>
class InputDoubleBuffer{
public:
  InputDoubleBuffer(){}

  #pragma hls_design interface
  void CCS_BLOCK(run)(ac_channel<PackedInt<INPUT_PRECISION, 4> > &inputs_in, 
                      ac_channel<PackedInt<INPUT_PRECISION, IC0> > &inputs_out,
                      ac_channel<Params> &paramsIn)
    {

        Params params = paramsIn.read();

        inputDoubleBufferReaderParams.write(params);
        inputDoubleBufferWriterParams.write(params);

        inputDoubleBufferWriter.run(inputDoubleBufferWriterParams, inputs_in, mem);

        inputDoubleBufferReader.run(inputDoubleBufferReaderParams, mem, inputs_out);
    }

private:
    ac_channel<chanStruct<PackedInt<INPUT_PRECISION, IC0>, size> > mem;
    
    InputDoubleBufferWriter<size, IC0, OC0> inputDoubleBufferWriter;
    ac_channel<Params> inputDoubleBufferWriterParams;
    
    InputDoubleBufferReader<size, IC0, OC0> inputDoubleBufferReader;
    ac_channel<Params> inputDoubleBufferReaderParams;
};

#endif
