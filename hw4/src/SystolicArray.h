#ifndef SYSTOLIC_ARRAY_H
#define SYSTOLIC_ARRAY_H

#include "ProcessingElement.h"
#include "conv.h"
#include "Fifo.h"
#include "SystolicArrayCore.h"

// Include mc_scverify.h for CCS_* macros
#include <mc_scverify.h>

class SystolicArrayLooper
{
public:
    SystolicArrayLooper() {}

#pragma hls_design interface
void run(ac_channel<Params> &paramsIn,
         ac_channel<Params> &paramsOut,
         ac_channel<LoopIndices> &loopIndicesOut)
    {
        // -------------------------------
        // Generate the loop indices here for the systolic array.
        // Write the loop indices as well as the params out to channels.
        // Your code starts here

        Params params = paramsIn.read(); // read params

        for (uint_16 oy1_idx = 0; oy1_idx < params.OY1; oy1_idx++) {
            for (uint_16 ox1_idx = 0; ox1_idx < params.OX1; ox1_idx++) {
                for (uint_16 oc1_idx = 0; oc1_idx < params.OC1; oc1_idx++) {
                    for (uint_16 ic1_idx = 0; ic1_idx < params.IC1; ic1_idx++) {
                        for (uint_16 fy_idx = 0; fy_idx < params.FY; fy_idx++) {
                            for (uint_16 fx_idx = 0; fx_idx < params.FX; fx_idx++) {
                                LoopIndices loopIndices;
                                loopIndices.ic1_idx = ic1_idx;
                                loopIndices.fy_idx = fy_idx;
                                loopIndices.fx_idx = fx_idx;
                                loopIndicesOut.write(loopIndices); // write weight tile indices in order for the systolic array to digest
                                paramsOut.write(params);
                            }
                        }
                    }
                }
            }
        }

        // Your code ends here
        // -------------------------------
    }
};

template <typename IDTYPE, typename WDTYPE, typename ODTYPE, int OC0, int IC0>
class SystolicArrayWrapper
{
public:
    SystolicArrayWrapper(){}
    
#pragma hls_design interface
    void run(ac_channel<PackedInt<INPUT_PRECISION, IC0> > &input, 
             ac_channel<PackedInt<WEIGHT_PRECISION, OC0> > &weight, 
             ac_channel<PackedInt<OUTPUT_PRECISION, OC0> > &output,
             ac_channel<Params> &paramsIn)
    {
        systolicArrayLooper.run(paramsIn, paramsChannel, loopIndicesChannel);
        systolicArrayCore.run(input, weight, output, paramsChannel, loopIndicesChannel);
    }
private:
    SystolicArrayCore<IDTYPE, WDTYPE, ODTYPE, OC0, IC0> systolicArrayCore;
    SystolicArrayLooper systolicArrayLooper;
    ac_channel<Params> paramsChannel;
    ac_channel<LoopIndices> loopIndicesChannel;
};

#endif
