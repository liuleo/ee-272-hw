import os
import sys

from mflowgen.components import Graph, Step

def construct():

  g = Graph()
  g.sys_path.append( '/farmshare/classes/ee/272' )

  #-----------------------------------------------------------------------
  # Parameters
  #-----------------------------------------------------------------------

  adk_name = 'skywater-130nm-adk.v2021'
  adk_view = 'view-standard'

  parameters = {
    'construct_path'  : __file__,
    'design_name'     : 'conv',
    'clock_period'    : 10.0,
    'adk'             : adk_name,
    'adk_view'        : adk_view,
    'topographical'   : True,
    'testbench_name'  : 'conv_tb',
    'strip_path'      : 'conv_tb/conv_inst',
    'saif_instance'   : 'conv_tb/conv_inst'
  }

  #-----------------------------------------------------------------------
  # Create nodes
  #-----------------------------------------------------------------------

  this_dir = os.path.dirname( os.path.abspath( __file__ ) )

  # ADK step

  g.set_adk(adk_name)
  adk = g.get_adk_step()

  # Custom steps

  sram = Step(this_dir + '/sram')
  layers = Step(this_dir + '/layers')
  rtl = Step(this_dir + '/rtl')
  testbench = Step(this_dir + '/testbench')
  unit_tests = Step(this_dir + '/unit_tests')
  constraints = Step(this_dir + '/constraints')

  # sim steps
  layers_list = ["conv1", "conv2_x", "conv3_1", "conv3_x", "conv4_1", "conv4_x", "conv5_1", "conv5_x"]
  sim_steps = list()
  for layer in layers_list:
    sim_step = Step(this_dir + '/synopsys-vcs-sim') # custom so we can dump run log
    sim_step.set_name("sim_{}".format(layer))
    sim_steps.append(sim_step)

  # Default steps

  info = Step('info', default=True)
 
  # gen saif steps
  gen_saif_steps = list()
  for layer in layers_list:
    gen_saif_step = Step('synopsys-vcd2saif-convert', default=True)
    gen_saif_step.set_name("gen_saif_{}".format(layer))
    gen_saif_steps.append(gen_saif_step)

  # dc steps (each with different activity file)
  dc_steps = list()
  for layer in layers_list:
    dc_step = Step('synopsys-dc-synthesis', default=True)
    dc_step.set_name("dc-synthesis_{}".format(layer))
    dc_steps.append(dc_step)

  #-----------------------------------------------------------------------
  # Graph -- Add nodes
  #-----------------------------------------------------------------------

  g.add_step(info)
  g.add_step(layers)
  g.add_step(sram)
  g.add_step(rtl)
  g.add_step(testbench)
  g.add_step(unit_tests)
  g.add_step(constraints)
  for sim_step in sim_steps:
    g.add_step(sim_step)
  for gen_saif_step in gen_saif_steps:
    g.add_step(gen_saif_step)
  for dc_step in dc_steps:
    g.add_step(dc_step)

  #-----------------------------------------------------------------------
  # Graph -- Add edges
  #-----------------------------------------------------------------------

  # unit tests

  g.connect_by_name(rtl, unit_tests)
  g.connect_by_name(testbench, unit_tests)

  # testbench

  for layer, sim_step, gen_saif_step in zip(layers_list, sim_steps, gen_saif_steps):
    # extend sim step to take SRAM models
    sim_step.extend_inputs(['sky130_sram_4kbyte_1rw1r_32x1024_8.v'])
    sim_step.extend_inputs(['sky130_sram_2kbyte_1rw1r_32x512_8.v'])
    sim_step.extend_inputs(['sky130_sram_1kbyte_1rw1r_32x256_8.v'])

    # extend sim step to take in driver and gold data as well as layer parameters
    sim_step.extend_inputs(["ifmap_data.txt", "weight_data.txt", "ofmap_data.txt", "layer_params.v"])
    g.connect(layers.o("{}_ifmap_data.txt".format(layer)), sim_step.i("ifmap_data.txt"))
    g.connect(layers.o("{}_weight_data.txt".format(layer)), sim_step.i("weight_data.txt"))
    g.connect(layers.o("{}_ofmap_data.txt".format(layer)), sim_step.i("ofmap_data.txt"))
    g.connect(layers.o("resnet_{}_params.v".format(layer)), sim_step.i("layer_params.v"))

    # connect testbench
    g.connect(testbench.o("design.args"), sim_step.i("design.args"))
    g.connect(testbench.o("conv_tb.v"), sim_step.i("testbench.sv"))

    # connect output vcd to this layer's gen saif step
    g.connect(sim_step.o('run.vcd'), gen_saif_step.i('run.vcd'))

    g.connect_by_name(rtl, sim_step)
    g.connect_by_name(sram, sim_step)

  # synthesis

  # synthesis

  for layer, dc_step, gen_saif_step in zip(layers_list, dc_steps, gen_saif_steps):
    dc_step.extend_inputs(['sky130_sram_4kbyte_1rw1r_32x1024_8_TT_1p8V_25C.db'])
    dc_step.extend_inputs(['sky130_sram_2kbyte_1rw1r_32x512_8_TT_1p8V_25C.db'])
    dc_step.extend_inputs(['sky130_sram_1kbyte_1rw1r_32x256_8_TT_1p8V_25C.db'])
    
    g.connect_by_name(adk, dc_step)
    g.connect_by_name(sram, dc_step)
    g.connect_by_name(rtl, dc_step)
    g.connect_by_name(constraints, dc_step)

    # connect output saif to this layer's synthesis step
    g.connect(gen_saif_step.o('run.saif'), dc_step.i('run.saif'))

  #-----------------------------------------------------------------------
  # Parameterize
  #-----------------------------------------------------------------------

  g.update_params(parameters)

  return g

if __name__ == '__main__':
  g = construct()




  dc.extend_inputs(['sky130_sram_4kbyte_1rw1r_32x1024_8_TT_1p8V_25C.db'])
  dc.extend_inputs(['sky130_sram_2kbyte_1rw1r_32x512_8_TT_1p8V_25C.db'])
  dc.extend_inputs(['sky130_sram_1kbyte_1rw1r_32x256_8_TT_1p8V_25C.db'])

  rtl_sim_vcs.extend_inputs(['layers'])
  rtl_sim_vcs.extend_inputs(['sky130_sram_4kbyte_1rw1r_32x1024_8.v'])
  rtl_sim_vcs.extend_inputs(['sky130_sram_2kbyte_1rw1r_32x512_8.v'])
  rtl_sim_vcs.extend_inputs(['sky130_sram_1kbyte_1rw1r_32x256_8.v'])
