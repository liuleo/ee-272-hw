#!/bin/bash

if grep -q "Error" tb_output.txt; then
    echo "UVM errors found"
    exit -1
else
    echo "No UVM errors"
fi

if grep -q "Offending" tb_output.txt; then
    echo "TB errors found"
    exit -1
else
    echo "No TB errors"
fi
