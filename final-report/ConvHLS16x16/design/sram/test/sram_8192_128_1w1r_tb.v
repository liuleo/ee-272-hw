`define ADDR_WIDTH 13
`define DATA_WIDTH 128
`define RAM_DEPTH 8192

module sram_8192_128_1w1r_tb;

  reg clk;
  reg csb0;
  reg web0;
  reg [`ADDR_WIDTH - 1 : 0] addr0;
  reg [`DATA_WIDTH - 1 : 0] din0;
  reg csb1;
  reg [`ADDR_WIDTH - 1 : 0] addr1;

  wire [`DATA_WIDTH - 1 : 0] dout1;

  always #10 clk =~clk;

  sram_8192_128_1w1r sram_8192_128_1w1r_i (
    .clk0(clk),
    .csb0(csb0),
    .web0(web0),
    .addr0(addr0),
    .din0(din0),
    .clk1(clk),
    .csb1(csb1),
    .addr1(addr1),
    .dout1(dout1)
  );

  integer x;

  initial begin
    clk <= 0;
    web0 <= 0;  // active low
    csb0 <= 1;
    csb1 <= 1;

    @(negedge clk)

    // write data
    csb0 <= 0;
    for (x = 0; x < `RAM_DEPTH; x = x + 1) begin
      addr0 <= x;
      din0 <= x;
      @(negedge clk);
    end

    csb0 <= 1;

    // read data
    csb1 <= 0;

    for (x = 0; x < `RAM_DEPTH; x = x + 1) begin
      addr1 <= x;
      if (x != 0) begin
        #3;
        assert(dout1 == x - 1) else $display("%t: addr = %d, data = %d", $time, x - 1, dout1);
      end
      @(negedge clk);
    end
    csb1 <= 1;

    $finish;
  end
  // initial begin
      // $vcdplusfile("dump.vcd");
      // $vcdplusmemon();
      // $vcdpluson(0, sram_8192_128_1w1r_tb);
      // #1000000;
      // $finish(2);
  // end

endmodule