#! /usr/bin/env python
#=========================================================================
# construct.py
#=========================================================================
# HLS DNN accelerator
#
# Author : Allen Pan
# Date   : Feb 16, 2022
#


import os
import sys

from mflowgen.components import Graph, Step

def construct():

  g = Graph()
  g.sys_path.append( '/farmshare/classes/ee/272' )

  #-----------------------------------------------------------------------
  # Parameters
  #-----------------------------------------------------------------------
  
  adk_name = 'skywater-130nm-adk.v2021'
  adk_view = 'view-standard'

  parameters = {
   'construct_path'  : __file__,
    'design_name'    : 'Conv',
    'clock_period'   : 10.0,
    'adk'            : adk_name,
    'adk_view'       : adk_view,
    'topographical'  : True,
    'strip_path'     : 'scverify_top/Conv',
    'saif_instance'  : 'scverify_top/Conv'
  }


  #-----------------------------------------------------------------------
  # Create nodes
  #-----------------------------------------------------------------------

  this_dir = os.path.dirname( os.path.abspath( __file__ ) )

  # ADK step

  g.set_adk(adk_name)
  adk = g.get_adk_step()

  # Custom steps

  sram = Step(this_dir + '/sram')
  hls = Step(this_dir + '/mentor-graphics-catapult-hls')
  hls._config['sandbox'] = False
  constraints = Step(this_dir + '/constraints')
  dc_gen_results = Step(this_dir + '/dc-generate-results')

  # Default steps

  info = Step('info', default=True)
  gen_saif_rtl = Step('synopsys-vcd2saif-convert', default=True)
  dc = Step('synopsys-dc-synthesis', default=True)

  #-----------------------------------------------------------------------
  # Graph -- Add nodes
  #-----------------------------------------------------------------------

  g.add_step(info)
  g.add_step(sram)
  g.add_step(hls)
  g.add_step(constraints)
  g.add_step(gen_saif_rtl)
  g.add_step(dc_gen_results)
  g.add_step(dc)
  #-----------------------------------------------------------------------
  # Graph -- Add edges
  #-----------------------------------------------------------------------
  
  # Dynamically add edges
  dc.extend_inputs(['generate-results.tcl'])
  dc.extend_inputs(['sky130_sram_4kbyte_1rw1r_32x1024_8_TT_1p8V_25C.db'])
  dc.extend_inputs(['sky130_sram_2kbyte_1rw1r_32x512_8_TT_1p8V_25C.db'])
  dc.extend_inputs(['sky130_sram_1kbyte_1rw1r_32x256_8_TT_1p8V_25C.db'])

  # Connect by name
  g.connect_by_name(sram,hls)
  g.connect_by_name(adk, dc)
  g.connect_by_name(sram, dc)
  g.connect_by_name(hls, dc)
  g.connect_by_name(constraints,dc)
  g.connect_by_name(dc_gen_results,dc)
  g.connect(hls.o('Conv.vcd' ), gen_saif_rtl.i( 'run.vcd' ) )
  g.connect_by_name(gen_saif_rtl, dc) # run.saif

  #-----------------------------------------------------------------------
  # Parameterize
  #-----------------------------------------------------------------------

  g.update_params(parameters)

  return g

if __name__ == '__main__':
  g = construct()
  g.plot()
