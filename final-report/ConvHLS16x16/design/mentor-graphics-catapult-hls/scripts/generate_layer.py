import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument("-l", "--layers", type=str, nargs='*', help='Layer specification files to run', default=["./layers/small_layer1.json"])

args = parser.parse_args()

with open(args.layers[0]) as f:
  data = json.load(f)

  param_str_c = f'''const int IC0 = {data["IC0"]};
const int OC0 = {data["OC0"]};
const int IC1 = {data["IC1"]};
const int OC1 = {data["OC1"]};
const int FX = {data["FX"]};
const int FY = {data["FY"]};
const int OX0 = {data["OX0"]};
const int OY0 = {data["OY0"]};
const int OX1 = {data["OX1"]};
const int OY1 = {data["OY1"]};
const int STRIDE = {data["STRIDE"]}; 
'''

with open("./src/conv_tb_params.h", "w") as output:
  output.write(param_str_c)
