#! /usr/bin/env bash
cd build/Conv.v1/

line=$(grep -n 'endmodule' concat_sim_rtl.v | tail -n1 | cut -f1 -d:)

sed -i "$line i\\
\\
  initial begin\\
      \$dumpfile(\"Conv.vcd\");\\
      \$dumpvars(0, Conv);\\
      #200000000;\\
      \$finish(2);\\
  end\\
" concat_sim_rtl.v
