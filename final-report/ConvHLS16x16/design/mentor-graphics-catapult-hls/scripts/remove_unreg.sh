#! /usr/bin/env bash
cd build/Conv.v1/

if [ $(grep 'module unreg_hier (' concat_rtl.v | wc -l) > 1 ]; then
  start=$(grep -n 'module unreg_hier (' concat_rtl.v |  head -n1 | cut -f1 -d:)
  end=$(grep -n -A10 'module unreg_hier (' concat_rtl.v | head -n11 | tail -n1 | cut -f1 -d-)
  sed -i "${start},${end}d" concat_rtl.v
fi

