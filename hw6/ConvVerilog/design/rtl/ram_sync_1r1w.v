module ram_sync_1r1w
#(
  parameter DATA_WIDTH = 32, // assume this is evenly divisible by 32
  parameter ADDR_WIDTH = 8,
  parameter DEPTH = 256     // assume this is >= 32
)(
  input clk,
  input wen,
  input [ADDR_WIDTH - 1 : 0] wadr,
  input [DATA_WIDTH - 1 : 0] wdata,
  input ren,
  input [ADDR_WIDTH - 1 : 0] radr,
  output [DATA_WIDTH - 1 : 0] rdata
);

  localparam integer NBANKS = $ceil(DEPTH/256);       // how many SRAM banks (size 256 each) do we need?
  localparam integer NCOLS = $ceil(DATA_WIDTH/32);    // how wide is each word in units of 32b?

  wire [DATA_WIDTH - 1 : 0] rdata_w [NBANKS-1:0];
  reg [ADDR_WIDTH - 1 : 0] radr_bank_num_reg;
  wire [ADDR_WIDTH - 1 : 0] wadr_bank_num;
  wire [ADDR_WIDTH - 1 : 0] radr_bank_num;

  generate
    if (ADDR_WIDTH <= 8) begin
      assign wadr_bank_num = 'b0; 
      assign radr_bank_num = 'b0; 
    end else begin
      assign wadr_bank_num = wadr[ADDR_WIDTH-1:8];
      assign radr_bank_num = radr[ADDR_WIDTH-1:8];
    end
  endgenerate

  genvar d, w;
  generate
    for (d = 0; d < NBANKS; d++) begin
      for (w = 0; w < NCOLS; w++) begin

        sky130_sram_1kbyte_1rw1r_32x256_8 sram_macro (
          .clk0(clk),
          .csb0(wadr_bank_num != d),
          .web0(!wen),
          .wmask0(4'hF),
          .addr0(wadr[7:0]),
          .din0(wdata[(w+1)*32-1:w*32]),
          .dout0(),
          .clk1(clk),
          .csb1(radr_bank_num != d),
          .addr1(radr[7:0]),
          .dout1(rdata_w[d][(w+1)*32-1:w*32])
        );

      end
    end
  endgenerate

  generate
    if (ADDR_WIDTH <= 8) begin
      always @(posedge clk) begin
        radr_bank_num_reg <= 'b0;
      end
    end else begin
      always @(posedge clk) begin
        radr_bank_num_reg <= radr[ADDR_WIDTH-1:8];
      end
    end
  endgenerate

  assign rdata = rdata_w[radr_bank_num_reg];

endmodule

// OpenRAM SRAM model
// Words: 256
// Word size: 32
// Write size: 8
// synopsys translate_off
module sky130_sram_1kbyte_1rw1r_32x256_8#(
  parameter NUM_WMASKS = 4,  // not needed as parameter
  parameter DATA_WIDTH = 32, // not needed as parameter
  parameter ADDR_WIDTH = 8,  // not needed as parameter
  parameter RAM_DEPTH = 256, // not needed as parameter
  parameter DELAY = 0        // nanoseconds?
)(
  input clk0, // clock
  input csb0, // active low chip select
  input web0, // active low write control
  input [NUM_WMASKS-1:0] wmask0, // write mask
  input [ADDR_WIDTH-1:0] addr0,
  input [DATA_WIDTH-1:0] din0,
  output reg [DATA_WIDTH-1:0] dout0,
  input clk1, // clock
  input csb1, // active low chip select
  input [ADDR_WIDTH-1:0] addr1,
  output reg [DATA_WIDTH-1:0] dout1
);

  reg csb0_reg;
  reg web0_reg;
  reg [NUM_WMASKS-1:0] wmask0_reg;
  reg [ADDR_WIDTH-1:0] addr0_reg;
  reg [DATA_WIDTH-1:0] din0_reg;

  reg csb1_reg;
  reg [ADDR_WIDTH-1:0] addr1_reg;
  reg [DATA_WIDTH-1:0] mem [0:RAM_DEPTH-1];

  // All inputs are registers
  always @(posedge clk0)
  begin
    csb0_reg = csb0;
    web0_reg = web0;
    wmask0_reg = wmask0;
    addr0_reg = addr0;
    din0_reg = din0;
    dout0 = 32'bx;
    // if ( !csb0_reg && web0_reg )
    //   $display($time," Reading %m addr0=%x dout0=%x",addr0_reg,mem[addr0_reg]);
    // if ( !csb0_reg && !web0_reg )
    //   $display($time," Writing %m addr0=%x din0=%x wmask0=%b",addr0_reg,din0_reg,wmask0_reg);
  end

  // All inputs are registers
  always @(posedge clk1)
  begin
    csb1_reg = csb1;
    addr1_reg = addr1;
    // if (!csb0 && !web0 && !csb1 && (addr0 == addr1))
    //   $display($time," WARNING: Writing and reading addr0=%x and addr1=%x simultaneously!",addr0,addr1);
    dout1 = 32'bx;
    // if ( !csb1_reg )
    //   $display($time," Reading %m addr1=%x dout1=%x",addr1_reg,mem[addr1_reg]);
  end


  // Memory Write Block Port 0
  // Write Operation : When web0 = 0, csb0 = 0
  always @ (negedge clk0)
  begin : MEM_WRITE0
    if ( !csb0_reg && !web0_reg ) begin
      if (wmask0_reg[0])
        mem[addr0_reg][7:0] = din0_reg[7:0];
      if (wmask0_reg[1])
        mem[addr0_reg][15:8] = din0_reg[15:8];
      if (wmask0_reg[2])
        mem[addr0_reg][23:16] = din0_reg[23:16];
      if (wmask0_reg[3])
        mem[addr0_reg][31:24] = din0_reg[31:24];
    end
  end

  // Memory Read Block Port 0
  // Read Operation : When web0 = 1, csb0 = 0
  always @ (negedge clk0)
  begin : MEM_READ0
    if (!csb0_reg && web0_reg)
      dout0 <= #(DELAY) mem[addr0_reg];
  end

  // Memory Read Block Port 1
  // Read Operation : When web1 = 1, csb1 = 0
  always @ (negedge clk1)
  begin : MEM_READ1
    if (!csb1_reg)
      dout1 <= #(DELAY) mem[addr1_reg];
  end

endmodule
// synopsys translate_on
