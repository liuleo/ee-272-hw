#! /usr/bin/env python
#=========================================================================
# construct.py
#=========================================================================
# ConvVerilog mflowgen
#
# Author : Leo Liu and Leo Joseph Henken
# Date   : February 15, 2021
#

import os
import sys

from mflowgen.components import Graph, Step

def construct():

  g = Graph()
  g.sys_path.append( '/farmshare/classes/ee/272' )

  #-----------------------------------------------------------------------
  # Parameters
  #-----------------------------------------------------------------------

  adk_name = 'skywater-130nm-adk.v2021'
  adk_view = 'view-standard'

  parameters = {
  'construct_path'  : __file__,
  'design_name'     : 'conv',
  'adk'             : adk_name,
  'adk_view'        : adk_view,
  'topographical'   : True,
  'testbench_name' : 'conv_tb',
  'strip_path'     : 'conv_tb/conv_inst',
  'saif_instance'  : 'conv_tb/conv_inst'
  }

  #-----------------------------------------------------------------------
  # Create nodes
  #-----------------------------------------------------------------------

  this_dir = os.path.dirname( os.path.abspath( __file__ ) )

  # ADK step

  g.set_adk(adk_name)
  adk = g.get_adk_step()

  # Custom steps

  sram = Step(this_dir + '/sram')
  layers = Step(this_dir + '/layers')
  rtl = Step(this_dir + '/rtl')
  testbench = Step(this_dir + '/testbench')
  unit_tests = Step(this_dir + '/unit_tests')
  constraints = Step(this_dir + '/constraints')

  LAYERS = ["conv1", "conv2_x", "conv3_1", "conv3_x", "conv4_1", "conv4_x", "conv5_1", "conv5_x"]
  # LAYERS = ["conv4_1"]

  sim_steps = list()
  for layer in LAYERS:
    sim_step = Step('synopsys-vcs-sim', default=True)
    sim_step.set_name("sim_{}".format(layer))
    sim_steps.append(sim_step)

  info = Step('info', default=True)
  gen_saif = Step('synopsys-vcd2saif-convert', default=True)
  dc = Step('synopsys-dc-synthesis', default=True)

  #-----------------------------------------------------------------------
  # Graph -- Add nodes
  #-----------------------------------------------------------------------

  g.add_step(info)
  g.add_step(layers)
  g.add_step(sram)
  g.add_step(rtl)
  g.add_step(testbench)
  g.add_step(unit_tests)
  g.add_step(constraints)
  g.add_step(gen_saif)
  g.add_step(dc)

  for layer, sim_step in zip(LAYERS, sim_steps):
    g.add_step(sim_step)

  #-----------------------------------------------------------------------
  # Graph -- Add edges
  #-----------------------------------------------------------------------

  dc.extend_inputs(['sram_tt_1p8V_25C.db'])

  g.connect_by_name(rtl, unit_tests)
  g.connect_by_name(testbench, unit_tests)

  for layer, sim_step in zip(LAYERS, sim_steps):
    sim_step.extend_inputs(["ifmap_data.txt", "weight_data.txt", "ofmap_data.txt", "layer_params.v"])
    g.connect_by_name(rtl, sim_step)
    g.connect(layers.o("{}_ifmap_data.txt".format(layer)), sim_step.i("ifmap_data.txt"))
    g.connect(layers.o("{}_weight_data.txt".format(layer)), sim_step.i("weight_data.txt"))
    g.connect(layers.o("{}_ofmap_data.txt".format(layer)), sim_step.i("ofmap_data.txt"))
    g.connect(testbench.o("design.args"), sim_step.i("design.args"))
    g.connect(testbench.o("conv_tb.v"), sim_step.i("testbench.sv"))
    g.connect(layers.o("resnet_{}_params.v".format(layer)), sim_step.i("layer_params.v"))

  g.connect(sim_steps[0].o('run.vcd'), gen_saif.i('run.vcd'))    # Arbitrary?

  g.connect_by_name(adk, dc)
  g.connect_by_name(sram, dc)
  g.connect_by_name(rtl, dc)
  g.connect_by_name(constraints, dc)
  g.connect_by_name(gen_saif, dc)

  g.param_space(dc, "clock_period", [100.0, 20.0, 10.0, 5.0, 4.0, 2.0])

  #-----------------------------------------------------------------------
  # Parameterize
  #-----------------------------------------------------------------------

  g.update_params(parameters)

  return g

if __name__ == '__main__':
  g = construct()
  g.plot()
