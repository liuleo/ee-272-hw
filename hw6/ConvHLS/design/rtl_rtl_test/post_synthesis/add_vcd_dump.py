with open("build/Conv.v1/concat_sim_rtl.v", "r") as f:
    i = 0
    content = f.readlines()

in_conv = False
insert_point = None
for line in content:
    i += 1
    line = line.strip()

    if line.startswith("module Conv ("):
        in_conv = True
    elif in_conv and line.startswith("endmodule"):
        in_conv = False
        insert_point = i-1

content.insert(insert_point, "end\n")
content.insert(insert_point, "$dumpvars(0, Conv);\n")
content.insert(insert_point, "$dumpfile(\"run.vcd\");\n")
content.insert(insert_point, "initial begin\n")

with open("build/Conv.v1/concat_sim_rtl.v", "w") as f:
    for line in content:
        f.write(line)
