#! /usr/bin/env python
#=========================================================================
# construct.py
#=========================================================================
# ConvHLS 
#
# Author : Leo Joseph Henken
# Date   : February 15, 2021
#

import os
import sys

from mflowgen.components import Graph, Step

def construct():

  g = Graph()
  g.sys_path.append( '/farmshare/classes/ee/272' )

  #-----------------------------------------------------------------------
  # Parameters
  #-----------------------------------------------------------------------
  
  adk_name = 'skywater-130nm-adk.v2021'
  adk_view = 'view-standard'

  parameters = {
    'construct_path' : __file__,
    'design_name'    : 'Conv',
    'adk'            : adk_name,
    'adk_view'       : adk_view,
    'topographical'  : True,
    'strip_path'     : 'scverify/Conv',
    'saif_instance'  : 'scverify/Conv'
  }

  #-----------------------------------------------------------------------
  # Create nodes
  #-----------------------------------------------------------------------

  this_dir = os.path.dirname( os.path.abspath( __file__ ) )

  # ADK step

  g.set_adk( adk_name )
  adk = g.get_adk_step()

  # Custom steps

  sram          = Step( this_dir + '/sram'        )
  rtl_rtl_test  = Step( this_dir + '/rtl_rtl_test')
  constraints   = Step( this_dir + '/constraints' )

  # Default steps

  info          = Step( 'info',                       default=True )
  gen_saif      = Step( 'synopsys-vcd2saif-convert',  default=True )
  dc            = Step( 'synopsys-dc-synthesis',      default=True )

  #-----------------------------------------------------------------------
  # Graph -- Add nodes
  #-----------------------------------------------------------------------

  g.add_step( info          )
  g.add_step( sram          )
  g.add_step( rtl_rtl_test  )
  g.add_step( gen_saif      )
  g.add_step( constraints   )
  g.add_step( dc            )

  #-----------------------------------------------------------------------
  # Graph -- Add edges
  #-----------------------------------------------------------------------
  
  # Dynamically add edges

  dc.extend_inputs(['sky130_sram_1kbyte_1rw1r_32x256_8_tt_1p8V_25C.db', 'sky130_sram_2kbyte_1rw1r_32x512_8_tt_1p8V_25C.db'])

  # Connect by name

  g.connect_by_name( adk,          dc           )
  g.connect_by_name( sram,         dc           )
  g.connect_by_name( rtl_rtl_test, dc           )
  g.connect_by_name( constraints,  dc           )
  g.connect( rtl_rtl_test.o( 'run.vcd' ), gen_saif.i( 'run.vcd' ) )
  g.connect_by_name( gen_saif, dc           ) # run.saif

  g.param_space(dc, "clock_period", [100.0, 20.0, 10.0, 5.0, 4.0, 2.0])

  #-----------------------------------------------------------------------
  # Parameterize
  #-----------------------------------------------------------------------

  g.update_params( parameters )

  return g

if __name__ == '__main__':
  g = construct()
  g.plot()
