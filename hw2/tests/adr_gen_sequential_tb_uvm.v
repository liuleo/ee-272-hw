// Write a UVM test for the sequential address generator (adr_gen_sequential).
// Especially make sure that the address generator works correctly with random
// stalls (meaning if adr_en goes low intermittently), that it resets properly, 
// and that it goes back to zero after reaching the maximum configured value.

// Your code starts here

// Do not change these.
`define BANK_ADDR_WIDTH (32)

// For coverage, you can change these.
`define TEST_LENGTH (100000)
`define CONFIG_DATA (100) // integer [0 ... 2^BANK_ADDR_WIDTH)
`define ADR_EN_HIGH_PERCENT 50 // integer [0, 100]
`define RST_N_LOW_PERCENT 10 // integer [0, 100]

// Sequential address generator unit interface; defines ports to DUT.
interface adr_gen_sequential_if (input bit clk);
  logic rst_n;
  logic adr_en;
  logic [`BANK_ADDR_WIDTH - 1 : 0] adr;
  logic config_en;
  logic [`BANK_ADDR_WIDTH - 1 : 0] config_data;
endinterface;

// Generator item object; encapsulates state of the generator at some point in
// time. The constraints enforce some sort of distribution of necessary values
// if an instance is randomized.
class adr_gen_sequential_item;
  rand logic adr_en; // allows driver to drive address enable
  logic [`BANK_ADDR_WIDTH - 1 : 0] adr; // allows monitor to view output address
  rand logic [`BANK_ADDR_WIDTH - 1 : 0] config_data; // allows driver to drive config data
  logic rst_n; // allows monitor to capture a reset
  logic config_en; // allows monitor to capture a config change after a reset

  constraint adr_en_dist { adr_en dist {0 := 100 - `ADR_EN_HIGH_PERCENT, 1 := `ADR_EN_HIGH_PERCENT}; }
  constraint config_data_fixed { config_data == `CONFIG_DATA; } // take out randomness so we can have predictable coverage
endclass

// Driver; applies generated stimulus (generator items) to DUT.
class driver;
  virtual adr_gen_sequential_if vif; // gets connected to concrete interface in top level
  mailbox driver_mailbox; // connected to test module to receive item sequence

  task run();
    $display("T=%0t [driver] Starting ...", $time);
    @(negedge vif.clk); // would this be better with a clocking block?
    forever begin
      adr_gen_sequential_item item;
      driver_mailbox.get(item);

      $display("T=%0t [driver] Driving adr_en %0d config_data %0d", $time, item.adr_en, item.config_data);
      vif.adr_en = item.adr_en;
      vif.config_data = item.config_data;

      @(negedge vif.clk);
    end
  endtask 
endclass

// Monitor; captures data from interface and sends it to the scoreboard.
class monitor;
  virtual adr_gen_sequential_if vif;
  mailbox scoreboard_mailbox; // connected to scoreboard to send captured data

  task run();
    $display("T=%0t [monitor] Starting ...", $time);
    forever begin
      adr_gen_sequential_item item = new();

      @ (posedge vif.clk);  // wait for rising edge so that synchronous signals are updated
      item.adr_en = vif.adr_en;
      item.adr = vif.adr;
      item.config_en = vif.config_en;
      item.config_data = vif.config_data;
      item.rst_n = vif.rst_n;

      scoreboard_mailbox.put(item);
    end
  endtask
endclass

// Scoreboard; receives data from the monitor and checks correctness.
class scoreboard;
  mailbox scoreboard_mailbox;
  int next_item;
  int curr_config_block_max; // we need to keep track of the maximum currently in use
  reg [`BANK_ADDR_WIDTH - 1 : 0] expected_adr = 0; // 0 after reset

  task run();
    forever begin
      adr_gen_sequential_item item;

      // Get first item.
      scoreboard_mailbox.get(item);
      
      next_item = 1;
      while (next_item < `TEST_LENGTH) begin

        // Using the current item, determine expected address for next item.
        if (~item.rst_n) begin
          expected_adr = 0;
          curr_config_block_max = 0;
        end else begin
          if (item.adr_en) begin
            if (item.adr >= curr_config_block_max) begin
              expected_adr = 0;
            end else begin
              expected_adr = expected_adr + 1;
            end
          end else begin
            expected_adr = expected_adr;
          end

          // Update maximum if needed.
          if (item.config_en) begin
            curr_config_block_max = item.config_data;
          end

        end	

        // Test output using next item.
        scoreboard_mailbox.get(item);
        next_item = next_item + 1;

        if (item.adr != expected_adr) begin 
          $display("T=%0t [scoreboard] Error! Received = %h, expected = %h", $time, item.adr, expected_adr);
        end else begin
          $display("T=%0t [scoreboard] Pass! Received = %h, expected = %h", $time, item.adr, expected_adr);
        end

      end
      $finish;
    end
  endtask
endclass

// Environment class; contains all verification components.
class env;
  driver driver;
  monitor monitor;
  scoreboard scoreboard;
  mailbox scoreboard_mailbox;
  virtual adr_gen_sequential_if vif;

  function new();
    driver = new();
    monitor = new();
    scoreboard = new();
    scoreboard_mailbox = new();

    monitor.scoreboard_mailbox = scoreboard_mailbox;
    scoreboard.scoreboard_mailbox = scoreboard_mailbox;
  endfunction

  virtual task run();
    driver.vif = vif;
    monitor.vif = vif;

    // Execute verification components in cycle lockstep.
    fork
      driver.run();
      monitor.run();
      scoreboard.run();
    join_any
  endtask
endclass          

// UVM bench; executes verification environment component loops and generates
// stimulus.
class test;
  env env;
  mailbox driver_mailbox;
  int next_item;

  function new();
    driver_mailbox = new();
    env = new();
  endfunction

  virtual task run();
    env.driver.driver_mailbox = driver_mailbox; // link driver and test
    fork
      env.run();
    join_none

    gen_items();
  endtask

  virtual task gen_items();
    adr_gen_sequential_item item;
    $display("T=%0t [test] Start writing stimulus ...", $time);

    // The first item sets the maximum.
    item = new();
    item.randomize();
    item.adr_en = 0; // undo randomness to start things clean
    driver_mailbox.put(item);

    next_item = 1;
    while (next_item < `TEST_LENGTH) begin
      item = new();
      item.randomize();
      driver_mailbox.put(item);
      next_item = next_item + 1;
    end
  endtask
endclass

// Top level bench; instantiates DUT.
module adr_gen_sequential_tb_uvm;

  reg clk;
  reg rst_n;
  reg config_en;  // essentially tied to reset; we assume that are no spurious enables during generation

  // Generate clock.
  always #10 clk = ~clk;

  // Instantiate DUT and concrete interface to DUT.
  adr_gen_sequential_if _if(clk);
  adr_gen_sequential #(
    .BANK_ADDR_WIDTH(`BANK_ADDR_WIDTH)
  )
  dut 
  (
    .clk(_if.clk),
    .rst_n(_if.rst_n),
    .adr_en(_if.adr_en),
    .adr(_if.adr),
    .config_en(_if.config_en),
    .config_data(_if.config_data)
  );

  assign _if.rst_n = rst_n;
  assign _if.config_en = config_en;

  initial begin
    test test;

    test = new();
    test.env.vif = _if;
    test.run();

    // Reset DUT and configure generator.
    #10
    clk <= 1;
    rst_n <= 0;
    #20
    config_en <= 1;
    rst_n <= 1;
    #20
    config_en <= 0;

    // Every so often pulse reset on negedge (according to distribution). On
    // the cycle after a reset, assert the config enable.
    while (1) begin
      if ($urandom_range(1,100) <= `RST_N_LOW_PERCENT) begin
        rst_n <= 0;
        #20
        config_en <= 1;
        rst_n <= 1;
        #20
        config_en <= 0;
      end 
      #20;
    end
  end

  initial begin
    $vcdplusfile("dump.vcd");
    $vcdplusmemon();
    $vcdpluson(0, adr_gen_sequential_tb_uvm);
  end
endmodule

// Your code ends here