// Write a UVM test for the MAC unit. Especially make sure that the MAC unit
// works correctly with random stalls (meaning if en goes low intermittently)
// and that it resets properly.

// Your code starts here

// Do not change these.
`define IFMAP_WIDTH (16)
`define WEIGHT_WIDTH (16) 
`define OFMAP_WIDTH (32)

// For coverage, you can change these.
`define TEST_LENGTH (100000)
`define EN_HIGH_PERCENT 99 // integer [0, 100]
`define WEIGHT_WEN_HIGH_PRECENT 5 // integer (0, 100]
`define RST_N_LOW_PERCENT 10 // integer [0, 100]

// MAC unit interface; defines ports to DUT.
interface mac_if (input bit clk);
  logic rst_n;
  logic en;
  logic weight_wen;
  logic signed [`IFMAP_WIDTH - 1 : 0] ifmap_in;
  logic signed [`WEIGHT_WIDTH - 1 : 0] weight_in;
  logic signed [`OFMAP_WIDTH - 1 : 0] ofmap_in;
  logic signed [`IFMAP_WIDTH - 1 : 0] ifmap_out;
  logic signed [`OFMAP_WIDTH - 1 : 0] ofmap_out;
endinterface;

// MAC item object; encapsulates state of the MAC unit at some point in time. 
// The constraints enforce some sort of distribution of necessary values if an
// instance is randomized.
class mac_item;
  rand logic en;
  rand logic weight_wen;
  rand logic signed [`IFMAP_WIDTH - 1 : 0] ifmap_in;
  rand logic signed [`WEIGHT_WIDTH - 1 : 0] weight_in;
  rand logic signed [`OFMAP_WIDTH - 1 : 0] ofmap_in;
  logic signed [`IFMAP_WIDTH - 1 : 0] ifmap_out;
  logic signed [`OFMAP_WIDTH - 1 : 0] ofmap_out;
  logic rst_n; // allows monitor to capture a reset

  constraint en_dist { en dist {0 := 100 - `EN_HIGH_PERCENT, 1 := `EN_HIGH_PERCENT}; }
  constraint weight_wen_dist { weight_wen dist {0 := 100 - `WEIGHT_WEN_HIGH_PRECENT, 1 := `WEIGHT_WEN_HIGH_PRECENT}; }
endclass

// Driver; applies generated stimulus (MAC items) to DUT.
class driver;
  virtual mac_if vif; // gets connected to concrete interface in top level
  mailbox driver_mailbox; // connected to test module to receive item sequence

  task run();
    $display("T=%0t [driver] Starting ...", $time);
    @(negedge vif.clk); // would this be better with a clocking block?
    forever begin
      mac_item item;
      driver_mailbox.get(item);

      $display("T=%0t [driver] Driving en %0d weight_wen %0d ifmap_in %0d weight_in %0d ofmap_in %0d", $time, item.en, item.weight_wen, item.ifmap_in, item.weight_in, item.ofmap_in);
      vif.en = item.en;
      vif.weight_wen = item.weight_wen;
      vif.ifmap_in = item.ifmap_in;
      vif.weight_in = item.weight_in;
      vif.ofmap_in = item.ofmap_in;

      @(negedge vif.clk);
    end
  endtask 
endclass

// Monitor; captures data from interface and sends it to the scoreboard.
class monitor;
  virtual mac_if vif;
  mailbox scoreboard_mailbox; // connected to scoreboard to send captured data

  task run();
    $display("T=%0t [monitor] Starting ...", $time);
    forever begin
      mac_item item = new();

      @ (posedge vif.clk);  // wait for rising edge so that synchronous signals are updated
      item.en = vif.en;
      item.weight_wen = vif.weight_wen;
      item.ifmap_in = vif.ifmap_in;
      item.weight_in = vif.weight_in;
      item.ofmap_in = vif.ofmap_in;
      item.ifmap_out = vif.ifmap_out;
      item.ofmap_out = vif.ofmap_out;
      item.rst_n = vif.rst_n;

      scoreboard_mailbox.put(item);
    end
  endtask
endclass

// Scoreboard; receives data from the monitor and checks correctness.
class scoreboard;
  mailbox scoreboard_mailbox;
  int next_item;
  int curr_weight; // we need to keep track of the weight being used in the operation
  reg [`OFMAP_WIDTH - 1 : 0] expected_ofmap_out;
  reg [`IFMAP_WIDTH - 1 : 0] expected_ifmap_out;

  task run();
    forever begin
      mac_item item;

      // Get first item.
      scoreboard_mailbox.get(item);

      next_item = 1;
      while (next_item < `TEST_LENGTH) begin

        // Using current item, determine expected outputs.
        if (~item.rst_n) begin
          expected_ifmap_out = 0;
          expected_ofmap_out = 0;
          curr_weight = 0;
        end else begin
          if (~item.en) begin
            expected_ifmap_out = item.ifmap_out;
            expected_ofmap_out = item.ofmap_out;
          end else begin
            expected_ifmap_out = item.ifmap_in;
            expected_ofmap_out = (curr_weight * item.ifmap_in) + item.ofmap_in;
          end

          // Update weight if needed.
          if (item.weight_wen) begin
            curr_weight = item.weight_in;
          end
        end

        // Test output using next item.
        scoreboard_mailbox.get(item);
        next_item = next_item + 1;

        if (item.ifmap_out != expected_ifmap_out) begin 
          $display("T=%0t [scoreboard] Error with ifmap_out! Received = %h, expected = %h", $time, item.ifmap_out, expected_ifmap_out);
        end else begin
          $display("T=%0t [scoreboard] Pass with ifmap_out! Received = %h, expected = %h", $time, item.ifmap_out, expected_ifmap_out);
        end

        if (item.ofmap_out != expected_ofmap_out) begin 
          $display("T=%0t [scoreboard] Error with ofmap_out! Received = %h, expected = %h", $time, item.ofmap_out, expected_ofmap_out);
        end else begin
          $display("T=%0t [scoreboard] Pass with ofmap_out! Received = %h, expected = %h", $time, item.ofmap_out, expected_ofmap_out);
        end
        
      end
      $finish;
    end
  endtask
endclass

// Environment class; contains all verification components.
class env;
  driver driver;
  monitor monitor;
  scoreboard scoreboard;
  mailbox scoreboard_mailbox;
  virtual mac_if vif;

  function new();
    driver = new();
    monitor = new();
    scoreboard = new();
    scoreboard_mailbox = new();

    monitor.scoreboard_mailbox = scoreboard_mailbox;
    scoreboard.scoreboard_mailbox = scoreboard_mailbox;
  endfunction

  virtual task run();
    driver.vif = vif;
    monitor.vif = vif;

    // Execute verification components in cycle lockstep.
    fork
      driver.run();
      monitor.run();
      scoreboard.run();
    join_any
  endtask
endclass          

// UVM bench; executes verification environment component loops and generates
// stimulus.
class test;
  env env;
  mailbox driver_mailbox;
  int next_item;

  function new();
    driver_mailbox = new();
    env = new();
  endfunction

  virtual task run();
    env.driver.driver_mailbox = driver_mailbox; // link driver and test
    fork
      env.run();
    join_none

    gen_items();
  endtask

  virtual task gen_items();
    mac_item item;
    $display("T=%0t [test] Start writing stimulus ...", $time);

    next_item = 0;
    while (next_item < `TEST_LENGTH) begin
      item = new();
      item.randomize();
      next_item = next_item + 1;
      driver_mailbox.put(item);
    end
  endtask
endclass

// Top level bench; instantiates DUT.
module mac_tb_uvm;

  reg clk;
  reg rst_n;

  // Generate clock.
  always #10 clk = ~clk;

  // Instantiate DUT and concrete interface to DUT.
  mac_if _if(clk);
  mac #(
    .IFMAP_WIDTH(`IFMAP_WIDTH),
    .WEIGHT_WIDTH(`WEIGHT_WIDTH),
    .OFMAP_WIDTH(`OFMAP_WIDTH)
  )
  dut 
  (
    .clk(_if.clk),
    .rst_n(_if.rst_n),
    .en(_if.en),
    .weight_wen(_if.weight_wen),
    .ifmap_in(_if.ifmap_in),
    .weight_in(_if.weight_in),
    .ofmap_in(_if.ofmap_in),
    .ifmap_out(_if.ifmap_out),
    .ofmap_out(_if.ofmap_out)
  );

  assign _if.rst_n = rst_n;

  initial begin
    test test;

    clk <= 0;
    rst_n <= 0;
    #20 rst_n <= 1;
    test = new();
    test.env.vif = _if;
    test.run();

    // Every so often toggle reset on negedge (according to distribution).
    #10;
    while (1) begin
      if ($urandom_range(0,100) <= `RST_N_LOW_PERCENT) begin
        rst_n <= 0;
      end else begin
        rst_n <= 1;
      end
      #20;
    end
  end

  initial begin
    $vcdplusfile("dump.vcd");
    $vcdplusmemon();
    $vcdpluson(0, mac_tb_uvm);
  end
endmodule

// Your code ends here