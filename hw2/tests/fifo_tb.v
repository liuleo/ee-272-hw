`define DATA_WIDTH 4
`define FIFO_DEPTH 3
`define COUNTER_WIDTH 1

module fifo_tb;

  // Write five directed tests for the fifo module that test different corner
  // cases. For example, whether it raises the empty and full flags correctly,
  // whether it clears (empties) when you assert the clr signal. Verify its
  // behaviour on reset. You should also test whether the fifo gives the
  // expected latency between when a data goes in and the earliest it can come
  // out. 

  // Your code starts here

  localparam DATA_WIDTH = 8;
  localparam FIFO_DEPTH = 4;
  localparam COUNTER_WIDTH = FIFO_DEPTH;

  reg clk;
  reg rst_n;
  reg enq, deq, clr;
  wire full_n, empty_n;

  reg [DATA_WIDTH-1:0] din;
  wire [DATA_WIDTH-1:0] dout;

  fifo #(
    .DATA_WIDTH(DATA_WIDTH),
    .FIFO_DEPTH(FIFO_DEPTH),
    .COUNTER_WIDTH(COUNTER_WIDTH)
  ) dut (
    .clk    (clk),
    .rst_n  (rst_n),
    .din    (din),
    .enq    (enq),
    .full_n (full_n),
    .dout   (dout),
    .deq    (deq),
    .empty_n(empty_n),
    .clr    (clr)
  );

  always #10 clk = ~clk;

  initial begin
    clk = 0;
    rst_n = 0;

    enq = 0;
    deq = 0;
    clr = 0;

    din = 0;

    // (1) Clear FIFO on reset, assert empty
    #20;
    rst_n = 1;
    #20;
    assert(~empty_n);
    assert(full_n);

    // (2) Write [AB CD EF FF] and assert FIFO full
    #20;
    enq = 1;
    din = 'hAB;

    #20;
    assert(empty_n);
    enq = 1;
    din = 'hCD;

    #20;
    enq = 1;
    din = 'hEF;

    #20;
    enq = 1;
    din = 'hFF;

    #20;
    enq = 0;
    assert(~full_n);

    // (3) Dequeue entire FIFO and assert FIFO values and empty
    #20;
    deq = 1;
    assert(dout == 'hAB);

    #20;
    deq = 1;
    assert(dout == 'hCD);
    assert(full_n);

    #20;
    deq = 1;
    assert(dout == 'hEF);

    #20;
    deq = 1;
    assert(dout == 'hFF);

    #20;
    deq = 0;
    assert(~empty_n);

    // (4) Enqueue a single element and verify readout latency of 2 cycles
    #20;
    enq = 1;
    din = 'h01;

    #20;
    deq = 1;
    enq = 0;
    assert(empty_n);

    #20;
    deq = 0;
    assert(dout == 'h01);

    #20;
    assert(~empty_n);

    // (5) Enqueue a single element and then clear, verifying 1 latency clear, and then fill FIFO all the way up again
    #20;
    enq = 1;
    din = 'h01;

    #20;
    enq = 0;
    clr = 1;

    #20;
    clr = 0;
    assert(~empty_n);

    #20;
    enq = 1;
    din = 'hAB;

    #20;
    assert(empty_n);
    enq = 1;
    din = 'hCD;

    #20;
    enq = 1;
    din = 'hEF;

    #20;
    enq = 1;
    din = 'hFF;

    #20;
    enq = 0;
    assert(~full_n);

  end



  // Your code ends here

  initial begin
    $vcdplusfile("dump.vcd");
    $vcdplusmemon();
    $vcdpluson(0, fifo_tb);
    #20000000;
    $finish(2);
  end

endmodule
