// Write a directed test for the accumulation buffer module. Make sure you test 
// all its ports and its behaviour when your switch banks.

// Your code starts here

module accumulation_buffer_tb;

  localparam BANK_ADDR_WIDTH = 2;
  localparam DATA_WIDTH = 8;
  localparam [BANK_ADDR_WIDTH:0] BANK_DEPTH = 4;

  reg clk;
  reg rst_n;
  reg switch_banks;

  reg ren;
  reg [BANK_ADDR_WIDTH-1:0] radr;
  wire [DATA_WIDTH-1:0] rdata;

  reg wen;
  reg [BANK_ADDR_WIDTH-1:0] wadr;
  reg [DATA_WIDTH-1:0] wdata;

  reg ren_wb;
  reg [BANK_ADDR_WIDTH-1:0] radr_wb;
  wire [DATA_WIDTH-1:0] rdata_wb;

  always #10 clk =~clk;

  accumulation_buffer #(
    .DATA_WIDTH     (DATA_WIDTH),
    .BANK_ADDR_WIDTH(BANK_ADDR_WIDTH),
    .BANK_DEPTH     (BANK_DEPTH)
  ) dut (
    .clk         (clk),
    .rst_n       (rst_n),
    .switch_banks(switch_banks),
    .ren         (ren),
    .radr        (radr),
    .rdata       (rdata),
    .wen         (wen),
    .wadr        (wadr),
    .wdata       (wdata),
    .ren_wb      (ren_wb),
    .radr_wb     (radr_wb),
    .rdata_wb    (rdata_wb)
  );

  initial begin
    clk = 0;

    // Initialize enables and addresses.
    ren = 0;
    wen = 0;
    ren_wb = 0;
    radr = 0;
    wadr = 0;
    radr_wb = 0;

    // Toggle reset
    rst_n = 0;
    #20;
    rst_n = 1;
    #20

    // Write bank 0: [AB CD EF FF] and bank 1: [67 45 23 01]
    wen = 1;
    wadr = 0;
    wdata = 'hAB;
    #20;

    wadr = 1;
    wdata = 'hCD;
    #20;

    wadr = 2;
    wdata = 'hEF;
    #20;

    switch_banks = 1; // will not register until next cycle
    wadr = 3;
    wdata = 'hFF;
    #20;

    switch_banks = 0;
    wadr = 3;
    wdata = 'h01;
    #20;

    wadr = 2;
    wdata = 'h23;
    #20;

    wadr = 1;
    wdata = 'h45;
    #20;

    switch_banks = 1; // will not register until next cycle
    wadr = 0;
    wdata = 'h67;
    #20;

    switch_banks = 0;
    wen = 0;
    ren = 1;
    radr = 0;
    ren_wb = 1;
    radr_wb = 0;
    #20;
    assert(rdata == 'hAB);
    assert(rdata_wb == 'h67);

    radr = 1;
    radr_wb = 1;
    #20;
    assert(rdata == 'hCD);
    assert(rdata_wb == 'h45);

    radr = 2;
    radr_wb = 2;
    #20;
    assert(rdata == 'hEF);
    assert(rdata_wb == 'h23);

    switch_banks = 1; // will not register until next cycle
    radr = 3;
    radr_wb = 3;
    #20;
    assert(rdata == 'hFF);
    assert(rdata_wb == 'h01);

    switch_banks = 0;
    radr = 0;
    radr_wb = 0;
    #20;
    assert(rdata == 'h67);
    assert(rdata_wb == 'hAB);

    radr = 1;
    radr_wb = 1;
    #20;
    assert(rdata == 'h45);
    assert(rdata_wb == 'hCD);

    radr = 2;
    radr_wb = 2;
    #20;
    assert(rdata == 'h23);
    assert(rdata_wb == 'hEF);

    radr = 3;
    radr_wb = 3;
    #20;
    assert(rdata == 'h01);
    assert(rdata_wb == 'hFF);

    $display("Test finished!");
    $finish();
  end

  initial begin
    $vcdplusfile("dump.vcd");
    $vcdplusmemon();
    $vcdpluson(0, accumulation_buffer_tb);
  end

endmodule


// Your code ends here
