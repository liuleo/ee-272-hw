module accumulation_buffer
#( 
  parameter DATA_WIDTH = 64,
  parameter BANK_ADDR_WIDTH = 7,
  parameter [BANK_ADDR_WIDTH : 0] BANK_DEPTH = 128
)(
  input clk,
  input rst_n,
  input switch_banks,
  
  input ren,
  input [BANK_ADDR_WIDTH - 1 : 0] radr,
  output [DATA_WIDTH - 1 : 0] rdata,
  
  input wen,
  input [BANK_ADDR_WIDTH - 1 : 0] wadr,
  input [DATA_WIDTH - 1 : 0] wdata,

  input ren_wb,
  input [BANK_ADDR_WIDTH - 1 : 0] radr_wb,
  output [DATA_WIDTH - 1 : 0] rdata_wb
);

  // Implement an accumulation buffer with the dual-port SRAM (ram_sync_1r1w)
  // provided. This SRAM allows one read and one write every cycle. To read
  // from it you need to supply the address on radr and turn ren (read enable)
  // high. The read data will appear on rdata port after 1 cycle (1 cycle
  // latency). To write into the SRAM, provide write address and data on wadr
  // and wdata respectively and turn write enable (wen) high. 
  
  // Accumulation buffer is similar to a double buffer, but one of its banks
  // has both a read port (ren, radr, rdata) and a write port (wen, wadr,
  // wdata). This bank is used by the systolic array to store partial sums and
  // then read them back out. The other bank has a read port only (ren_wb,
  // radr_wb, rdata_wb). This bank is used to read out the final output (after
  // accumulation is complete) and send it out of the chip. The reason for
  // adopting two banks is so that we can overlap systolic array processing,
  // and data transfer out of the accelerator (otherwise one of them will
  // stall while the other is taking place). Note: both srams will be 1r1w, 
  // but the logical operation will be as described above.

  // If switch_banks is high, you need to switch the functionality of the two
  // banks at the positive edge of the clock. That means, you will use the bank
  // you were previously using for data transfer out of the chip for systolic
  // array and vice versa.

  // Your code starts here

  reg b_sel; // the bank with current dual-ported operation
  reg ram_wen[1:0], ram_ren[1:0];
  reg [BANK_ADDR_WIDTH-1:0] ram_radr[1:0], ram_wadr[1:0];
  reg [DATA_WIDTH-1:0] ram_wdata[1:0];
  wire [DATA_WIDTH-1:0] ram_rdata[1:0];

  always @(*) begin
    ram_wen[b_sel] = wen;
    ram_wadr[b_sel] = wadr;
    ram_wdata[b_sel] = wdata;
    ram_ren[b_sel] = ren;
    ram_radr[b_sel] = radr;

    ram_wen[~b_sel] = 0;
    ram_wadr[~b_sel] = 0;
    ram_wdata[~b_sel] = 0;
    ram_ren[~b_sel] = ren_wb;
    ram_radr[~b_sel] = radr_wb;
  end

  always @(posedge clk) begin
    if (~rst_n) begin
      b_sel <= 0;
    end else if (switch_banks) begin
      b_sel <= ~b_sel;
    end
  end

	// Note that the read data outputs are continuously assigned by a mux. This 
	// means that if you assert a read enable on the same cycle as a bank switch,
	// read data output glitches and resolves to the data last read from the new
	// bank, not the data from the previous bank as was the case in the single 
	// SRAM double buffer. This is most definitely not desired, so it is best to 
	// not assert either read enable on the cycle that the switch banks flag is
	// asserted.

  assign rdata = switch_banks ? ram_rdata[~b_sel] : ram_rdata[b_sel];
  assign rdata_wb = switch_banks ? ram_rdata[b_sel] : ram_rdata[~b_sel];

  ram_sync_1r1w #(
    .DATA_WIDTH(DATA_WIDTH),
    .ADDR_WIDTH(BANK_ADDR_WIDTH),
    .DEPTH(BANK_DEPTH)
  ) bank0 (
    .clk  (clk),
    .wen  (ram_wen[0]),
    .wadr (ram_wadr[0]),
    .wdata(ram_wdata[0]),
    .ren  (ram_ren[0]),
    .radr (ram_radr[0]),
    .rdata(ram_rdata[0])
  );

  ram_sync_1r1w #(
    .DATA_WIDTH(DATA_WIDTH),
    .ADDR_WIDTH(BANK_ADDR_WIDTH),
    .DEPTH(BANK_DEPTH)
  ) bank1 (
    .clk  (clk),
    .wen  (ram_wen[1]),
    .wadr (ram_wadr[1]),
    .wdata(ram_wdata[1]),
    .ren  (ram_ren[1]),
    .radr (ram_radr[1]),
    .rdata(ram_rdata[1])
  );

  // Your code ends here
endmodule
