module ifmap_radr_gen
#( 
  parameter BANK_ADDR_WIDTH = 8
)(
  input clk,
  input rst_n,
  input adr_en,
  output [BANK_ADDR_WIDTH - 1 : 0] adr,
  input config_en,
  input [BANK_ADDR_WIDTH*9 - 1 : 0] config_data
);

  reg [BANK_ADDR_WIDTH - 1 : 0] config_OX0, config_OY0, config_FX, config_FY, 
    config_STRIDE, config_IX0, config_IY0, config_IC1, config_OC1;

  always @ (posedge clk) begin
    if (rst_n) begin
      if (config_en) begin
        {config_OX0, config_OY0, config_FX, config_FY, config_STRIDE,
         config_IX0, config_IY0, config_IC1, config_OC1} <= config_data;
      end
    end else begin
      {config_OX0, config_OY0, config_FX, config_FY, config_STRIDE,
       config_IX0, config_IY0, config_IC1, config_OC1} <= 0;
    end
  end
  
  // This is the read address generator for the input double buffer. It is
  // more complex than the sequential address generator because there are
  // overlaps between the input tiles that are read out.  We have already
  // instantiated for you all the configuration registers that will hold the
  // various tiling parameters (OX0, OY0, FX, FY, STRIDE, IX0, IY0, IC1, OC1).
  // You need to generate address (adr) for the input buffer in the same
  // sequence as the C++ tiled convolution that you implemented. Make sure you
  // increment/step the address generator only when adr_en is high. Also reset
  // all registers when rst_n is low.  
  
  // Your code starts here

  // for (int ic1 = 0; ic1 < IC1; ic1++) {
  //   for (int fy = 0; fy < FY; fy++) {
  //     for (int fx = 0; fx < FX; fx++) {
  //       for (int oy0 = 0; oy0 < OY0; oy0++) {
  //         for (int ox0 = 0; ox0 < OX0; ox0++) {
  //           ifmap[((oy1*OY0+oy0)*STRIDE)+fy][((ox1*OX0+ox0)*STRIDE)+fx][(ic1*IC0 + ic0)];
  //         }
  //       }
  //     }
  //   }
  // }

  reg ic1_en, fy_en, fx_en, oy0_en, ox0_en;
  reg [BANK_ADDR_WIDTH-1:0] ic1, fy, fx, oy0, ox0;

  always @(*) begin
    if (adr_en & rst_n) begin
      ox0_en = 1;
      oy0_en = ((ox0 == config_OX0-1) && (ox0_en)) ? 1 : 0;
      fx_en = ((oy0 == config_OY0-1) && (oy0_en)) ? 1 : 0;
      fy_en = ((fx == config_FX-1) && (fx_en)) ? 1 : 0;
      ic1_en = ((fy == config_FY-1) && (fy_en)) ? 1 : 0;
    end else begin
      ic1_en = 0;
      fy_en = 0;
      fx_en = 0;
      oy0_en = 0;
      ox0_en = 0;
    end
  end

  assign adr = (ox0*config_STRIDE)+fx + ((oy0*config_STRIDE)+fy)*config_IY0 + ic1*config_IX0*config_IY0;

  always_ff @(posedge clk) begin
    if (~rst_n) begin
      ic1 <= 0;
    end else begin
      if (ic1_en) begin
        ic1 <= (ic1 >= config_IC1-1) ? 0 : ic1 + 1;
      end else begin
        ic1 <= ic1;
      end
    end
  end


  always_ff @(posedge clk) begin
    if (~rst_n) begin
      fy <= 0;
    end else begin
      if (fy_en) begin
        fy <= (fy >= config_FY-1) ? 0 : fy + 1;
      end else begin
        fy <= fy;
      end
    end
  end


  always_ff @(posedge clk) begin
    if (~rst_n) begin
      fx <= 0;
    end else begin
      if (fx_en) begin
        fx <= (fx >= config_FX-1) ? 0 : fx + 1;
      end else begin
        fx <= fx;
      end
    end
  end


  always_ff @(posedge clk) begin
    if (~rst_n) begin
      oy0 <= 0;
    end else begin
      if (oy0_en) begin
        oy0 <= (oy0 >= config_OY0-1) ? 0 : oy0 + 1;
      end else begin
        oy0 <= oy0;
      end
    end
  end


  always_ff @(posedge clk) begin
    if (~rst_n) begin
      ox0 <= 0;
    end else begin
      if (ox0_en) begin
        ox0 <= (ox0 >= config_OX0-1) ? 0 : ox0 + 1;
      end else begin
        ox0 <= ox0;
      end
    end
  end


  // Your code ends here
endmodule