import os
import sys

from mflowgen.components import Graph, Step

def construct():

  g = Graph()
  g.sys_path.append( '/farmshare/classes/ee/272' )

  #-----------------------------------------------------------------------
  # Parameters
  #-----------------------------------------------------------------------

  adk_name = 'skywater-130nm-adk.v2021'
  adk_view = 'view-standard'

  parameters = {
    'construct_path' : __file__,
    'design_name'    : 'conv',
    'clock_period'   : 10.0, # this overrides other clock_period definitions
    'adk'            : adk_name,
    'adk_view'       : adk_view,
    'topographical'  : True,
    'testbench_name' : 'conv_tb',
    'strip_path'     : 'conv_tb/conv_inst',
    'saif_instance'  : 'conv_tb/conv_inst'
  }

  #-----------------------------------------------------------------------
  # Create nodes
  #-----------------------------------------------------------------------

  this_dir = os.path.dirname( os.path.abspath( __file__ ) )

  # ADK step

  g.set_adk( adk_name )
  adk = g.get_adk_step()

  # Custom steps

  sram         = Step( this_dir + '/sram'        )
  rtl          = Step( this_dir + '/rtl'         )
  testbench    = Step( this_dir + '/testbench'   )
  constraints  = Step( this_dir + '/constraints' )
  rtl_sim_vcs  = Step( this_dir + '/synopsys-vcs-sim')

  # Default steps

  info         = Step( 'info',                           default=True )
  dc           = Step( 'synopsys-dc-synthesis',          default=True )

  ########## Added steps for HW7 ##########

  iflow = Step('cadence-innovus-flowsetup', default=True)
  floorplan = Step(this_dir + '/floorplan')
  pin_placement = Step(this_dir + '/pin-placement')
  init = Step('cadence-innovus-init', default=True)
  power = Step(this_dir + '/cadence-innovus-power')
  place = Step('cadence-innovus-place', default=True)
  cts = Step('cadence-innovus-cts', default=True)
  postcts_hold = Step('cadence-innovus-postcts_hold', default=True)
  route = Step('cadence-innovus-route', default=True)
  postroute = Step('cadence-innovus-postroute', default=True)
  signoff = Step('cadence-innovus-signoff', default=True)

  #########################################

  #-----------------------------------------------------------------------
  # Graph -- Add nodes
  #-----------------------------------------------------------------------

  g.add_step( info         )
  g.add_step( sram         )
  g.add_step( rtl          )
  g.add_step( testbench    )
  g.add_step( constraints  )
  g.add_step( dc           )
  g.add_step( rtl_sim_vcs  )

  ########## Added steps for HW7 ##########

  g.add_step(iflow)
  g.add_step(floorplan)
  g.add_step(pin_placement)
  g.add_step(init)
  g.add_step(power)
  g.add_step(place)
  g.add_step(cts)
  g.add_step(postcts_hold)
  g.add_step(route)
  g.add_step(postroute)
  g.add_step(signoff)

  #########################################

  #-----------------------------------------------------------------------
  # Graph -- Add edges
  #-----------------------------------------------------------------------

  # Dynamically add edges
 
  dc.extend_inputs(['sky130_sram_4kbyte_1rw1r_32x1024_8_TT_1p8V_25C.db'])
  dc.extend_inputs(['sky130_sram_4kbyte_1rw1r_32x1024_8.lef'])
  dc.extend_inputs(['sky130_sram_2kbyte_1rw1r_32x512_8_TT_1p8V_25C.db'])
  dc.extend_inputs(['sky130_sram_2kbyte_1rw1r_32x512_8.lef'])
  dc.extend_inputs(['sky130_sram_1kbyte_1rw1r_32x256_8_TT_1p8V_25C.db'])
  dc.extend_inputs(['sky130_sram_1kbyte_1rw1r_32x256_8.lef'])

  rtl_sim_vcs.extend_inputs(['ifmap_data.txt'])
  rtl_sim_vcs.extend_inputs(['weight_data.txt'])
  rtl_sim_vcs.extend_inputs(['ofmap_data.txt'])
  rtl_sim_vcs.extend_inputs(['layer_params.v'])
  rtl_sim_vcs.extend_inputs(['sky130_sram_4kbyte_1rw1r_32x1024_8.v'])
  rtl_sim_vcs.extend_inputs(['sky130_sram_2kbyte_1rw1r_32x512_8.v'])
  rtl_sim_vcs.extend_inputs(['sky130_sram_1kbyte_1rw1r_32x256_8.v'])

  # extend saif out of synopsys simulation
  rtl_sim_vcs.extend_outputs(['run.saif'])

  ########## Added steps for HW7 ##########

  for step in [iflow, init, power, place, cts, postcts_hold, route, postroute, signoff]:
    step.extend_inputs(['sky130_sram_4kbyte_1rw1r_32x1024_8_TT_1p8V_25C.lib'])
    step.extend_inputs(['sky130_sram_4kbyte_1rw1r_32x1024_8.lef'])
    step.extend_inputs(['sky130_sram_2kbyte_1rw1r_32x512_8_TT_1p8V_25C.lib'])
    step.extend_inputs(['sky130_sram_2kbyte_1rw1r_32x512_8.lef'])
    step.extend_inputs(['sky130_sram_1kbyte_1rw1r_32x256_8_TT_1p8V_25C.lib'])
    step.extend_inputs(['sky130_sram_1kbyte_1rw1r_32x256_8.lef'])

  init.extend_inputs(['floorplan.tcl', 'pin-assignments.tcl'])

  #########################################

  # Connect by name
  g.connect_by_name( rtl,          rtl_sim_vcs  ) # design.v
  g.connect_by_name( sram,         rtl_sim_vcs  ) # design.v
  g.connect_by_name( testbench,    rtl_sim_vcs  ) # testbench.sv

  g.connect_by_name( rtl_sim_vcs,  dc           ) # run.saif
  g.connect_by_name( rtl,          dc           )
  g.connect_by_name( adk,          dc           )
  g.connect_by_name( constraints,  dc           )
  g.connect_by_name( sram,         dc           )

  ########## Added steps for HW7 ##########

  # iflow
  g.connect_by_name(adk, iflow)
  g.connect_by_name(dc, iflow)
  g.connect_by_name(sram, iflow)

  # init
  g.connect_by_name(adk, init)
  g.connect_by_name(dc, init)
  g.connect_by_name(iflow, init)
  g.connect_by_name(floorplan, init)
  g.connect_by_name(pin_placement, init)
  g.connect_by_name(sram, init)

  # power
  g.connect_by_name(adk, power)
  g.connect_by_name(dc, power)
  g.connect_by_name(iflow, power)
  g.connect_by_name(init, power)
  g.connect_by_name(sram, power)

  # place
  g.connect_by_name(adk, place)
  g.connect_by_name(dc, place)
  g.connect_by_name(iflow, place)
  g.connect_by_name(sram, place)
  g.connect_by_name(power, place)

  # cts
  g.connect_by_name(adk, cts)
  g.connect_by_name(dc, cts)
  g.connect_by_name(iflow, cts)
  g.connect_by_name(sram, cts)
  g.connect_by_name(place, cts)

  # postcts_hold
  g.connect_by_name(adk, postcts_hold)
  g.connect_by_name(iflow, postcts_hold)
  g.connect_by_name(sram, postcts_hold)
  g.connect_by_name(cts, postcts_hold)

  # route
  g.connect_by_name(adk, route)
  g.connect_by_name(iflow, route)
  g.connect_by_name(sram, route)
  g.connect_by_name(postcts_hold, route)

  # postroute
  g.connect_by_name(adk, postroute)
  g.connect_by_name(iflow, postroute)
  g.connect_by_name(sram, postroute)
  g.connect_by_name(route, postroute)

  # signoff
  g.connect_by_name(adk, signoff)
  g.connect_by_name(iflow, signoff)
  g.connect_by_name(sram, signoff)
  g.connect_by_name(postroute, signoff)

  #########################################

  #-----------------------------------------------------------------------
  # Parameterize
  #-----------------------------------------------------------------------

  g.update_params( parameters )

  return g

if __name__ == '__main__':
  g = construct()
  