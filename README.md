# ee-272-hw

Collection of code for homework deliverables in EE 272. Each HW gets its own folder.

**Run the following after cloning:**
```
git submodule init && git submodule update
```

